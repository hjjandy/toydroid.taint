package edu.purdue.cs.toydroid.taint;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Statement.Kind;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAFieldAccessInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.util.graph.Graph;

import edu.purdue.cs.toydroid.utils.WalaUtil;

public class TaintModel {
	
	public static int pathIdx = 0;

	public static void checkModel(Graph<Statement> sdg, ParamCaller pstmt,
			Map<SSAInstruction, String> transmission) {
		SSAAbstractInvokeInstruction inst = pstmt.getInstruction();
		int pval = pstmt.getValueNumber();
		if (pval != inst.getUse(0)) {
			return;
		}
		String sig = WalaUtil.getSignature(inst);// System.err.println(sig);
		if (!"android.os.AsyncTask.execute([Ljava/lang/Object;)Landroid/os/AsyncTask;".equals(sig)) {
			return;
		}

		Iterator<Statement> iter = sdg.getPredNodes(pstmt);
		while (iter.hasNext()) {
			Statement pred = iter.next();
			if (pred.getKind() == Kind.NORMAL) {
				NormalStatement ns = (NormalStatement) pred;
				SSAInstruction sinst = ns.getInstruction();
				if (sinst instanceof SSANewInstruction) {
					SSANewInstruction ninst = (SSANewInstruction) sinst;
					String type = ninst.getConcreteType().getName().toString();
					// System.err.println(type + "  " + ninst.get);
					if (type.contains("/")) {
						type = type.replace('/', '.');
						type = type.substring(1);
					}

					String target = String.format(
							"%s.doInBackground([Ljava/lang/Object;)Ljava/lang/Object;",
							type);
					transmission.put(pstmt.getInstruction(), target);
				}
			}
		}
	}

	public static boolean eliminateSpecialInterests(
			SSAFieldAccessInstruction inst) {
		String klass = inst.getDeclaredField()
				.getDeclaringClass()
				.getName()
				.toString();
		if (klass.startsWith("Ljava/util/")){
			return true;
		}
		return false;
	}

	public static boolean specialInterests(SSAAbstractInvokeInstruction inst) {
		MethodReference target = inst.getDeclaredTarget();
		String sig = target.getSignature();
		String name = target.getName().toString();
		if (sig.startsWith("java.util.")
				&& (name.startsWith("add") || name.startsWith("set")
						|| name.startsWith("put") || name.startsWith("offer") || name.startsWith("push"))) {
			return true;
		} else if (false && (sig.startsWith("java.lang.StringBuilder") || sig.startsWith("java.lang.StringBuffer"))
				&& (name.startsWith("append") || name.startsWith("set") || name.startsWith("insert"))) {
			return true;
		}
		return false;
	}

	private static Set<String> interestingModels;
	static {
		interestingModels = new java.util.HashSet<String>();
		interestingModels.add("Ljava/util/ArrayList");
		interestingModels.add("Ljava/util/ArrayDeque");
		interestingModels.add("Ljava/util/HashSet");
		interestingModels.add("Ljava/util/HashMap");
		interestingModels.add("Ljava/util/LinkedList");
		interestingModels.add("Ljava/util/LinkedHashMap");
		interestingModels.add("Ljava/util/LinkedHashSet");
		interestingModels.add("Ljava/util/Stack");
		interestingModels.add("Ljava/util/Vector");
		interestingModels.add("Ljava/util/Map");
		interestingModels.add("Ljava/util/List");
		interestingModels.add("Ljava/util/Set");
		//interestingModels.add("Ljava/lang/StringBuilder");
		//interestingModels.add("Ljava/util/StringBuffer");
	}

	public static boolean isInterestingModel(String str) {
		return interestingModels.contains(str);
	}
}
