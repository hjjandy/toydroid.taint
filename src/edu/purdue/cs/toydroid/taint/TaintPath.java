package edu.purdue.cs.toydroid.taint;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.wala.ipa.slicer.HeapStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.graph.impl.SparseNumberedGraph;
import com.ibm.wala.viz.NodeDecorator;

import edu.purdue.cs.toydroid.utils.WalaUtil;

public class TaintPath extends SparseNumberedGraph<TaintPathNode> {
	private static Logger logger = LogManager.getLogger(TaintPath.class);

	public Statement source;
	public Map<Statement, String> sink;
	private Map<Statement, TaintPathNode> stmt2Node;
	private boolean changed;
	private Set<Statement> fakeSource;

	public TaintPath(Statement src) {
		source = src;
		init();
	}

	public TaintPath() {
		init();
	}

	private void init() {
		sink = new HashMap<Statement, String>();
		stmt2Node = new HashMap<Statement, TaintPathNode>();
		fakeSource = new HashSet<Statement>();
	}

	public void addFakeSource(Statement s) {
		fakeSource.add(s);
	}

	public Iterator<Statement> iterateFakeSource() {
		return fakeSource.iterator();
	}

	public void addSink(Statement s, String tag) {
		sink.put(s, tag);
	}

	public int numOfSinks() {
		return sink.size();
	}

	public void setChanged(boolean c) {
		changed = c;
	}

	public boolean isChanged() {
		return changed;
	}

	public String getSinkTag(Statement s) {
		return sink.get(s);
	}

	public boolean hasSink() {
		return !sink.isEmpty();
	}

	// @Override
	// public void addEdge(TaintPathNode src, TaintPathNode dst) {
	// if (src != null && dst != null) {
	// super.addEdge(src, dst);
	// }
	// }

	public TaintPathNode addPathNode(Statement s) {
		TaintPathNode pn = new TaintPathNode(s);
		addNode(pn);
		stmt2Node.put(s, pn);
		if (s.equals(source)) {
			pn.setAsSource();
		} else if (sink.containsKey(s)) {
			pn.setAsSink();
		}
		addPathNodeExtra(s);
		changed = true;
		return pn;
	}

	public TaintPathNode getPathNode(Statement stmt) {
		return stmt2Node.get(stmt);
	}

	// subclasses may implement some interesting features.
	protected void addPathNodeExtra(Statement s) {

	}

	public static NodeDecorator<TaintPathNode> makeNodeDecorator() {
		return new NodeDecorator<TaintPathNode>() {

			@Override
			public String getLabel(TaintPathNode n) throws WalaException {
				Statement stmt = n.getStmt();
				return WalaUtil.stringForStmt(stmt);
			}

		};
	}

	private void removeCycledEdges(TaintPathNode node,
			Stack<TaintPathNode> visited, Set<TaintPathNode> allVisited) {
		visited.push(node);
		Iterator<TaintPathNode> iter = getSuccNodes(node);
		while (iter.hasNext()) {
			TaintPathNode pn = iter.next();
			if (visited.contains(pn)) {
				removeEdge(node, pn);
			} else if (!allVisited.contains(pn)) {
				allVisited.add(pn);
				removeCycledEdges(pn, visited, allVisited);
			}
		}
		visited.pop();
	}

	public void removeUnneccessaryNodes() {
		// logger.info("    - Try to remove unneccessary nodes in taint path.");
		List<TaintPathNode> worklist = new LinkedList<TaintPathNode>();
		TaintPathNode start;
		if (source != null) {
			start = stmt2Node.get(source);
			worklist.add(start);
		}
		Iterator<Statement> iterFSrc = iterateFakeSource();
		while (iterFSrc.hasNext()) {
			start = stmt2Node.get(iterFSrc.next());
			worklist.add(start);
		}

		Stack<TaintPathNode> visited = new Stack<TaintPathNode>();
		Set<TaintPathNode> allVisited = new HashSet<TaintPathNode>();
		while (!worklist.isEmpty()) {
			start = worklist.remove(0);
			removeCycledEdges(start, visited, allVisited);
		}

		Iterator<TaintPathNode> iter = iterator();
		while (iter.hasNext()) {
			TaintPathNode pn = iter.next();
			if (this.getSuccNodeCount(pn) == 0 && !pn.isSink()) {
				Iterator<TaintPathNode> it = this.getPredNodes(pn);
				while (it.hasNext()) {
					worklist.add(it.next());
				}
				this.removeNodeAndEdges(pn);
				stmt2Node.remove(pn.getStmt());
			}
		}

		while (!worklist.isEmpty()) {
			TaintPathNode pn = worklist.remove(0);
			if (!stmt2Node.containsKey(pn.getStmt()))
				continue;
			if (this.getSuccNodeCount(pn) == 0 && !pn.isSink()) {
				Iterator<TaintPathNode> it = this.getPredNodes(pn);
				while (it.hasNext()) {
					TaintPathNode tpn = it.next();
					if (!worklist.contains(tpn)) {
						worklist.add(tpn);
					}
				}
				this.removeNodeAndEdges(pn);
				stmt2Node.remove(pn.getStmt());
			}
		}
		// logger.info("    - Finish to remove unneccessary nodes in taint path.");
	}
}
