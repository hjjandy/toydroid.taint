package edu.purdue.cs.toydroid.taint;

import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.util.graph.impl.NodeWithNumber;

public class TaintPathNode extends NodeWithNumber {

	private Statement stmt;
	private int tag;

	public TaintPathNode(Statement s) {
		stmt = s;
		tag = 0;
	}

	public Statement getStmt() {
		return stmt;
	}

	public void setAsSource() {
		tag = 1;
	}

	public void setAsSink() {
		tag = 2;
	}

	public boolean isSource() {
		return tag == 1;
	}

	public boolean isSink() {
		return tag == 2;
	}
}
