package edu.purdue.cs.toydroid.taint;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.util.collections.Pair;

import edu.purdue.cs.toydroid.taint.SimplifiedTaintPath.SimplifiedPath;

public class SimplifiedResult {
	private static Map<Pair<String, String>, SimplifiedPath> allPaths;

	public static void newPaths(List<SimplifiedPath> paths) {
		if (allPaths == null) {
			allPaths = new HashMap<Pair<String, String>, SimplifiedPath>();
		}
		for (SimplifiedPath p : paths) {
			Pair<String, String> key = p.keyAsSourceAndSink();
			SimplifiedPath existingPath = allPaths.get(key);
			if (existingPath != null) {
				existingPath.merge(p);
			} else {
				allPaths.put(key, p);
			}
		}
	}

	public static void dumpPaths() {
		int idx = 0;
		boolean hasToArray = false;
		Set<Map.Entry<Pair<String, String>, SimplifiedPath>> eset = allPaths.entrySet();
		for (Map.Entry<Pair<String, String>, SimplifiedPath> e : eset) {
			SimplifiedPath path = e.getValue();
			hasToArray = (hasToArray | path.hasToArray);
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(idx + "."
						+ path.sinkTag + ".txt"));
				writer.write(path.toString());
				writer.flush();
			} catch (Exception ex) {

			} finally {
				if (writer != null) {
					try {
						writer.close();
					} catch (Exception ex) {

					}
				}
			}
			idx++;
		}
		if (hasToArray) {
			try {
				FileWriter fw = new FileWriter("HasToArray.txt");
				fw.write("toArray");
				fw.close();
			} catch (Exception e) {
				
			}
		}
	}
}
