package edu.purdue.cs.toydroid.taint;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;

import edu.purdue.cs.toydroid.taint.TaintMap;
import edu.purdue.cs.toydroid.utils.AnalysisConfig;

public class InterestingTaintNode {
	private SSAAbstractInvokeInstruction instr;
	private TaintSubMap subGraph;
	private int[] interestingIdx;
	public String tag; // tag for SINK

	InterestingTaintNode(SSAAbstractInvokeInstruction i, TaintSubMap sg,
			String interestingIdx) {
		instr = i;
		subGraph = sg;
		parseInterestingIndices(interestingIdx);
	}

	public static InterestingTaintNode getInstance(
			SSAAbstractInvokeInstruction i, TaintSubMap sg,
			String interestingIdx) {
		return new InterestingTaintNode(i, sg, interestingIdx);
	}

	public String sinkSignature() {
		return instr.getDeclaredTarget().getSignature();
	}

	public TaintSubMap enclosingTaintSubMap() {
		return subGraph;
	}

	public TaintMap enclosingTaintMap() {
		return subGraph.taintMap;
	}

	public Iterator<Integer> iterateInterestingArgs() {
		List<Integer> list = new ArrayList<Integer>();
		for (int i : interestingIdx) {
			int arg;
			if (i == -1)
				arg = instr.getDef();
			else
				arg = instr.getUse(i);
			list.add(arg);
		}
		return list.iterator();
	}

	private void parseInterestingIndices(String s) {
		String[] a = s.split(AnalysisConfig.SEPERATOR);
		interestingIdx = new int[a.length - 1];
		tag = a[0];
		int idx = 0;
		for (int i = 1; i < a.length; i++) {
			interestingIdx[idx] = Integer.parseInt(a[i]);
			idx++;
		}
	}

	public int hashCode() {
		return instr.hashCode();
	}

	public boolean equals(Object obj) {
		if (obj instanceof InterestingTaintNode) {
			return instr.equals(((InterestingTaintNode) obj).instr)
					&& subGraph.cgNode.equals(((InterestingTaintNode) obj).subGraph.cgNode);
		}
		return false;
	}
}
