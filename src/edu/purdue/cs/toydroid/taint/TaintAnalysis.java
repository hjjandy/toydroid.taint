package edu.purdue.cs.toydroid.taint;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.wala.dalvik.classLoader.DexIRFactory;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.AnalysisOptions.ReflectionOptions;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.InstanceFieldKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.callgraph.propagation.SSAPropagationCallGraphBuilder;
import com.ibm.wala.ipa.callgraph.propagation.StaticFieldKey;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.slicer.HeapStatement;
import com.ibm.wala.ipa.slicer.HeapStatement.HeapParamCallee;
import com.ibm.wala.ipa.slicer.HeapStatement.HeapReturnCallee;
import com.ibm.wala.ipa.slicer.NormalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCallee;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.PhiStatement;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Slicer.ControlDependenceOptions;
import com.ibm.wala.ipa.slicer.Slicer.DataDependenceOptions;
import com.ibm.wala.ipa.slicer.Statement.Kind;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.Predicate;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.graph.GraphSlicer;
import com.ibm.wala.viz.DotUtil;

import edu.purdue.cs.toydroid.taint.SimplifiedTaintPath.SimplifiedPath;
import edu.purdue.cs.toydroid.utils.AnalysisConfig;
import edu.purdue.cs.toydroid.utils.AnalysisScopeUtil;
import edu.purdue.cs.toydroid.utils.EntrypointUtil;
import edu.purdue.cs.toydroid.utils.ResourceUtil;
import edu.purdue.cs.toydroid.utils.SimpleCounter;
import edu.purdue.cs.toydroid.utils.WalaUtil;

public class TaintAnalysis implements Callable<TaintAnalysis> {

	private static Logger logger = LogManager.getLogger(TaintAnalysis.class);

	private static boolean taskTimeout = false;
	private static long timeout = 30;
	private static long taskStart;

	static String ApkFile = "E:\\Eclipse-Workspace\\TestAndroidAct\\bin\\TestAndroidAct.apk";

	// static String ApkFile = "E:\\x\\y\\AM-mnn.Android-3713.apk";

	// static String ApkFile = "E:\\x\\y\\AM-ru.qip-28.apk";

	// static String ApkFile = "E:\\x\\AM-Busan.Subway.f7key.Cauly-50.apk";

	// static String ApkFile = "E:\\x\\y\\AM-com.intsig.camscanner-28.apk";

	// static String ApkFile = "E:\\x\\y\\AM-mnn.Android-3713.apk";

	// static String ApkFile = "E:\\x\\y\\AM-com.bet.oneohsix-10205.apk";
	// static String ApkFile = "E:\\x\\AM-com.BPTracker-29.apk";
	// static String ApkFile =
	// "E:\\x\\AM-com.boursorama.android.clients-21.apk";

	// static String ApkFile = "E:\\x\\y\\AM-me.blip-100303.apk";

	public static void main(String[] args) {
		taskStart = System.currentTimeMillis();
		if (args.length == 1) {
			ApkFile = args[0];
		}
		try {
			doAnalysis(ApkFile);
		} catch (Throwable e) {
			logger.error("Crashed: {}", e.getMessage());
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		String time = String.format("%d.%03d", (end - taskStart) / 1000,
				(end - taskStart) % 1000);
		logger.info("Total Time: {} seconds.", time);
		System.exit(0);
	}

	public static void doAnalysis(String apk) throws Throwable {
		logger.info("Start Taint Analysis...");
		TaintAnalysis analysis = new TaintAnalysis(apk);
		Future<TaintAnalysis> future = Executors.newSingleThreadExecutor()
				.submit(analysis);
		try {
			try {
				future.get(timeout, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				logger.error("InterruptedException: {}", e.getMessage());
				e.printStackTrace();
			} catch (ExecutionException e) {
				logger.error("ExecutionException: {}", e.getMessage());
				e.printStackTrace();
			} catch (TimeoutException e) {
				taskTimeout = true;
				logger.warn("Analysis Timeout after {} {}!", timeout,
						TimeUnit.MINUTES);
			}
		} catch (Throwable t) {
			logger.error("Crashed: {}", t.getMessage());
			t.printStackTrace();
		}
		if (!future.isDone()) {
			future.cancel(true);
		}
		long analysisEnd = System.currentTimeMillis();
		String time = String.format("%d.%03d",
				(analysisEnd - taskStart) / 1000,
				(analysisEnd - taskStart) % 1000);
		logger.info("Total Analysis Time: {} seconds.", time);
		if (analysis.hasResult()) {
			analysis.dumpResult();
		}
	}

	private String apkFile;
	private AnalysisScope scope;
	private ClassHierarchy cha;
	private Map<Entrypoint, TaintMap> entry2TaintMap;
	private TaintMap currentTaintMap;
	private Map<Statement, InterestingTaintNode> sinks, sinksForCurrentEntry;
	private Map<Statement, InterestingTaintNode> srcs, srcsForCurrentEntry;
	private Map<SSAInstruction, String> interestingTransmissions;
	private List<TaintPath> allPaths;
	private List<Object> crossEntrypointRecord;

	public TaintAnalysis(String apk) {
		apkFile = apk;
		entry2TaintMap = new HashMap<Entrypoint, TaintMap>();
		sinks = new HashMap<Statement, InterestingTaintNode>();
		srcs = new HashMap<Statement, InterestingTaintNode>();
		sinksForCurrentEntry = new HashMap<Statement, InterestingTaintNode>();
		srcsForCurrentEntry = new HashMap<Statement, InterestingTaintNode>();
		interestingTransmissions = new HashMap<SSAInstruction, String>();
		allPaths = new LinkedList<TaintPath>();
		crossEntrypointRecord = new LinkedList<Object>();
	}

	public boolean hasResult() {
		return !allPaths.isEmpty();
	}

	@Override
	public TaintAnalysis call() throws Exception {
		initialize();
		analyze();
		// do taint track
		track();
		return this;
	}

	private void initialize() throws Exception {
		scope = AnalysisScopeUtil.makeAnalysisScope(apkFile);
		cha = ClassHierarchy.make(scope);

		WalaUtil.setClassHierarchy(cha);
		ResourceUtil.parse(apkFile, cha);
		Set<String> compClasses = ResourceUtil.getComponentClasses();

		EntrypointUtil.initialEntrypoints(cha, compClasses);
		EntrypointUtil.discoverNewEntrypoints(scope);
	}

	private void analyze() throws Exception {
		Entrypoint entrypoint;
		SSAPropagationCallGraphBuilder cgBuilder;
		CallGraph cg;

		AnalysisCache cache = new AnalysisCache(new DexIRFactory());
		ArrayList<Entrypoint> epList = new ArrayList<Entrypoint>(1);
		int nEntrypoints = EntrypointUtil.allEntrypointCount();
		int idxEntrypoint = 1;
		while (null != (entrypoint = EntrypointUtil.nextEntrypoint())) {
			if (taskTimeout) {
				break;
			}
			String epSig = entrypoint.getMethod().getSignature();
			if (epSig.startsWith("java.util.")
					|| epSig.startsWith("java.lang."))
				continue;
			// if (!epSig.contains(".TextMessenger.onClick(")
			// && !epSig.contains(".Store$DatabaseHelper.onUpgrade(")
			// && !epSig.contains("C2DMReceiver.onReceive(")) {
			// continue;
			// }
			logger.info("Process entrypoint ({}/{}) {}", idxEntrypoint++,
					nEntrypoints, epSig);
			epList.add(entrypoint);
			AnalysisOptions options = new AnalysisOptions(scope, epList);
			options.setReflectionOptions(ReflectionOptions.NONE);
			cgBuilder = Util.makeVanillaNCFABuilder(1, options, cache, cha,
					scope);
			logger.info(" * Build CallGraph");
			cg = cgBuilder.makeCallGraph(options, null);

			if (taskTimeout) {
				break;
			}

			logger.info(" * Build SDG");
			SDG sdg = new SDG(cg, cgBuilder.getPointerAnalysis(),
					DataDependenceOptions.NO_BASE_NO_EXCEPTIONS,
					ControlDependenceOptions.NONE);
			logger.info(" * SDG size before pruning: {}",
					sdg.getNumberOfNodes());
			if (taskTimeout) {
				break;
			}
			Graph<Statement> g = pruneSDG(sdg);
			logger.info(" * SDG size after pruning: {}", g.getNumberOfNodes());
			// DotUtil.dotify(g, WalaUtil.makeNodeDecorator(),
			// entrypoint.getMethod().getName().toString() + ".0.dot",
			// null, null);
			visitSDG(entrypoint, cg, g);
			// currentTaintMap = new TaintMap(entrypoint, sdg);
			// entry2TaintMap.put(entrypoint, currentTaintMap);
			SummaryGraph<Statement> sum = buildSummary(entrypoint, cg, g);
			logger.info("  * Summary size: {}", sum.getNumberOfNodes());
			currentTaintMap.sdg = sum;

			if (sum.getNumberOfNodes() == 0) {
				entry2TaintMap.remove(entrypoint);
			} else {
				// String name = String.format("%s.%d.dot",
				// entrypoint.getMethod()
				// .getName()
				// .toString(), idxEntrypoint);
				// DotUtil.dotify(sum, WalaUtil.makeNodeDecorator(), name, null,
				// null);

				srcs.putAll(srcsForCurrentEntry);
				sinks.putAll(sinksForCurrentEntry);
			}
			srcsForCurrentEntry.clear();
			sinksForCurrentEntry.clear();

			epList.clear();
		}
	}

	private void dumpResult() {
		logger.info("Start Dumping Results...");
		int idx = 0;
		for (TaintPath path : allPaths) {
			if (path instanceof SimplifiedTaintPath) {
				SimplifiedTaintPath stp = (SimplifiedTaintPath) path;
				List<SimplifiedPath> paths = stp.simplify();
				SimplifiedResult.newPaths(paths);
			}
			// try {
			// DotUtil.dotify(path, TaintPath.makeNodeDecorator(), idx
			// + ".dot", null, null);
			// idx++;
			// } catch (WalaException e) {
			// e.printStackTrace();
			// }
		}
		SimplifiedResult.dumpPaths();
		logger.info("Finish Dumping Results.");
	}

	private SummaryGraph<Statement> buildSummary(Entrypoint ep, CallGraph cg,
			Graph<Statement> sdg) {
		logger.info("  - Build SDG Summary");
		SummaryGraph<Statement> summary = new SummaryGraph<Statement>();
		if (!currentTaintMap.hasEntries()
				&& !currentTaintMap.hasIncomingFields()
				&& srcsForCurrentEntry.isEmpty()) {
			sinksForCurrentEntry.clear();
			return summary;
		}
		if (sinksForCurrentEntry.isEmpty()
				&& !currentTaintMap.hasOutgoingFields()) {
			srcsForCurrentEntry.clear();
			return summary;
		}
		Set<Statement> visitedHeapStmt = new HashSet<Statement>();
		List<Statement> worklist = new LinkedList<Statement>();
		Statement stmt;
		Iterator<Statement> iter = currentTaintMap.iterateEntries();
		while (iter.hasNext()) {
			stmt = iter.next();
			// if (!summary.containsNode(stmt)) {
			summary.addNode(stmt);
			// }
			TaintSubMap sm = currentTaintMap.findOrCreateSubMap(stmt.getNode());
			Iterator<Statement> newly = sm.iterateDislinkedPropagation(((ParamCallee) stmt).getValueNumber());
			while (newly.hasNext()) {
				Statement s = newly.next();
				if (!summary.containsNode(s)) {
					summary.addNode(s);
					worklist.add(s);
				}
				summary.addEdge(stmt, s);
			}
			// entry here does not have succ nodes.
		}
		iter = currentTaintMap.iterateAllIncomingFields();
		while (iter.hasNext()) {
			stmt = iter.next();
			summary.addNode(stmt);
			buildSummaryForStmt(cg, sdg, stmt, summary, worklist,
					visitedHeapStmt);
		}
		// iter = currentTaintMap.iterateAllOutgoingFields();
		// while (iter.hasNext()) {
		// stmt = iter.next();
		// buildSummaryForStmt(sdg, stmt, summary, worklist);
		// }
		iter = srcsForCurrentEntry.keySet().iterator();
		while (iter.hasNext()) {
			stmt = iter.next();
			summary.addNode(stmt);
			buildSummaryForStmt(cg, sdg, stmt, summary, worklist,
					visitedHeapStmt);
		}
		// iter = sinksForCurrentEntry.keySet().iterator();
		// while (iter.hasNext()) {
		// stmt = iter.next();
		// buildSummaryForStmt(sdg, stmt, summary, worklist);
		// }
		boolean hasSinkOrOutgoing = false;
		while (!worklist.isEmpty()) {
			stmt = worklist.remove(0);
			if (!hasSinkOrOutgoing) {
				if (stmt.getKind() == Kind.PARAM_CALLER
						&& sinksForCurrentEntry.containsKey(stmt)) {
					hasSinkOrOutgoing = true;
				} else if (stmt.getKind() == Kind.NORMAL) {
					NormalStatement nstmt = (NormalStatement) stmt;
					if (nstmt.getInstruction() instanceof SSAPutInstruction) {
						hasSinkOrOutgoing = true;
					}
				} else if (stmt.getKind() == Kind.PARAM_CALLER) {
					ParamCaller pc = (ParamCaller) stmt;
					if (interestingTransmissions.containsKey(pc.getInstruction())) {
						hasSinkOrOutgoing = true;
					}
				}
			}

			buildSummaryForStmt(cg, sdg, stmt, summary, worklist,
					visitedHeapStmt);

			Kind kind = stmt.getKind();
			if (kind == Kind.NORMAL) {
				NormalStatement nstmt = (NormalStatement) stmt;
				SSAInstruction inst = nstmt.getInstruction();
				if (inst instanceof SSAArrayStoreInstruction) {
					SSAArrayStoreInstruction arInst = (SSAArrayStoreInstruction) inst;
					TaintSubMap sm = currentTaintMap.findOrCreateSubMap(nstmt.getNode());
					iter = sm.iterateDislinkedPropagation(arInst.getArrayRef());
					while (iter.hasNext()) {
						Statement s = iter.next();
						if (!stmt.equals(s)) {
							if (!summary.containsNode(s)) {
								summary.addNode(s);
								worklist.add(s);
							}
							summary.addEdge(stmt, s);
						}
					}
				} else if (sdg.getSuccNodeCount(stmt) == 0 && inst.hasDef()) {
					// what happen? maybe LHS is an array which doesn't have a
					// succ node or it's arrayload in some case that no succ
					// connected.
					TaintSubMap sm = currentTaintMap.findOrCreateSubMap(stmt.getNode());
					iter = sm.iterateDislinkedPropagation(inst.getDef());
					while (iter.hasNext()) {
						Statement s = iter.next();
						if (!stmt.equals(s)) {
							if (!summary.containsNode(s)) {
								summary.addNode(s);
								worklist.add(s);
							}
							summary.addEdge(stmt, s);
						}
					}
				}
			} else if (kind == Kind.PARAM_CALLEE) {
				ParamCallee pc = (ParamCallee) stmt;
				int pval = pc.getValueNumber();
				if (sdg.getSuccNodeCount(stmt) == 0) {
					TaintSubMap sm = currentTaintMap.findOrCreateSubMap(pc.getNode());
					iter = sm.iterateDislinkedPropagation(pval);
					while (iter.hasNext()) {
						Statement s = iter.next();
						if (!summary.containsNode(s)) {
							summary.addNode(s);
							worklist.add(s);
						}
						summary.addEdge(stmt, s);
					}
				}
			} else if (kind == Kind.PARAM_CALLER) {
				if (sinksForCurrentEntry.containsKey(stmt)) {
					continue;
				}
				ParamCaller pc = (ParamCaller) stmt;
				if (sdg.getSuccNodeCount(pc) == 0) {
					int pval = pc.getValueNumber();
					TaintSubMap sm = currentTaintMap.findOrCreateSubMap(pc.getNode());
					iter = sm.iterateDislinkedPropagation(pval);
					while (iter.hasNext()) {
						Statement s = iter.next();
						if (s.getKind() == Kind.NORMAL_RET_CALLER) {
							NormalReturnCaller nstmt = (NormalReturnCaller) s;
							SSAAbstractInvokeInstruction nInst = nstmt.getInstruction();
							SSAAbstractInvokeInstruction pInst = pc.getInstruction();
							if (nInst.equals(pInst)) {
								if (!summary.containsNode(s)) {
									summary.addNode(s);
									worklist.add(s);
								}
								summary.addEdge(stmt, s);
								// } else if (!nInst.isStatic() &&
								// !pInst.isStatic()
								// && nInst.getUse(0) == pInst.getUse(0)) {
								// if (!summary.containsNode(s)) {
								// summary.addNode(s);
								// worklist.add(s);
								// }
								// summary.addEdge(stmt,
								// s);//System.err.println(pc+"\n  >> "+ nstmt);
							}
						}
					}
					if (!pc.getInstruction().isStatic()) {
						int thisVn = pc.getInstruction().getUse(0);
						if (thisVn != pval) {
							iter = sm.iterateDislinkedPropagation(thisVn);
							while (iter.hasNext()) {
								Statement s = iter.next();
								if (s.getKind() == Kind.PARAM_CALLEE) {
									ParamCaller npc = (ParamCaller) s;
									if (!npc.getInstruction().isStatic()
											&& thisVn != npc.getInstruction()
													.getUse(0)) {
										if (!summary.containsNode(s)) {
											summary.addNode(s);
											worklist.add(s);
										}
										summary.addEdge(stmt, s);
									}
								} else {
									if (!summary.containsNode(s)) {
										summary.addNode(s);
										worklist.add(s);
									}
									summary.addEdge(stmt, s);
								}
							}
						}
					}
				}
			} else if (kind == Kind.NORMAL_RET_CALLER) {
				// just in case. anyway, arrayload and getfield can sometimes
				// have no succs connected
				if (sdg.getSuccNodeCount(stmt) == 0) {
					CGNode cn = stmt.getNode();
					TaintSubMap sm = currentTaintMap.findOrCreateSubMap(cn);
					SSAInstruction inst = ((NormalReturnCaller) stmt).getInstruction();
					iter = sm.iterateDislinkedPropagation(inst.getDef());
					while (iter.hasNext()) {
						Statement s = iter.next();
						if (!summary.containsNode(s)) {
							summary.addNode(s);
							worklist.add(s);
						}
						summary.addEdge(stmt, s);
					}
				}
			}
		}
		if (!hasSinkOrOutgoing) {
			summary = null;
			summary = new SummaryGraph<Statement>();
		} else {
			removeUnusedNodesFromSummary(summary);
		}
		return summary;
	}

	private void removeUnusedNodesFromSummary(SummaryGraph<Statement> summary) {
		logger.info("      * Remove unneccessary nodes from summary.");
		List<Statement> worklist = new LinkedList<Statement>();
		for (Statement stmt : summary) {
			if (0 == summary.getSuccNodeCount(stmt)) {
				worklist.add(stmt);
			}
		}
		while (!worklist.isEmpty()) {
			Statement stmt = worklist.remove(0);
			if (!summary.containsNode(stmt)
					|| summary.getSuccNodeCount(stmt) != 0) {
				continue;
			}
			if (sinksForCurrentEntry.containsKey(stmt)) {
				continue;
			} else if (stmt.getKind() == Kind.NORMAL) {
				NormalStatement nstmt = (NormalStatement) stmt;
				if (nstmt.getInstruction() instanceof SSAPutInstruction) {
					continue;
				}
			} else if (stmt.getKind() == Kind.PARAM_CALLER) {
				if (interestingTransmissions.containsKey(((ParamCaller) stmt).getInstruction())) {
					continue;
				}
			}
			Iterator<Statement> iter = summary.getPredNodes(stmt);
			while (iter.hasNext()) {
				Statement st = iter.next();
				// if (!worklist.contains(st)) {
				worklist.add(st);
				// }
			}
			summary.removeNodeAndEdges(stmt);
			if (srcsForCurrentEntry.containsKey(stmt)) {
				srcsForCurrentEntry.remove(stmt);
			}
			currentTaintMap.removeIfExists(stmt);
		}
	}

	private void buildSummaryForStmt(CallGraph cg, Graph<Statement> sdg,
			Statement stmt, SummaryGraph<Statement> summary,
			List<Statement> worklist, Set<Statement> visitedHeapStmt) {
		// if (!summary.containsNode(stmt)) {
		// summary.addNode(stmt);
		// }
		Iterator<Statement> succs = sdg.getSuccNodes(stmt);
		while (succs.hasNext()) {
			Statement succ = succs.next();
			if (succ instanceof HeapStatement) {
				skipHeapStmtInSummary(cg, sdg, stmt, succ, summary, worklist,
						visitedHeapStmt);
			} else if (!cg.getFakeRootNode().equals(succ.getNode())) {
				if (!summary.containsNode(succ)) {
					summary.addNode(succ);
					worklist.add(succ);
				}
				summary.addEdge(stmt, succ);
			}
		}
	}

	private void skipHeapStmtInSummary(CallGraph cg, Graph<Statement> sdg,
			Statement src, Statement heapStmt, SummaryGraph<Statement> summary,
			List<Statement> worklist, Set<Statement> visited) {
		List<Statement> heapWorklist = new LinkedList<Statement>();
		heapWorklist.add(heapStmt);
		while (!heapWorklist.isEmpty()) {
			Statement hstmt = heapWorklist.remove(0);
			if (visited.contains(hstmt)) {
				continue;
			}
			visited.add(hstmt);
			Iterator<Statement> succs = sdg.getSuccNodes(hstmt);
			while (succs.hasNext()) {
				Statement succ = succs.next();
				if (succ instanceof HeapStatement) {
					heapWorklist.add(succ);
				} else if (!cg.getFakeRootNode().equals(succ.getNode())) {
					if (!summary.containsNode(succ)) {
						summary.addNode(succ);
						worklist.add(succ);
					}
					summary.addEdge(src, succ);
				}
			}
		}
	}

	private void track() {
		logger.info("Start Taint Track for {} sources and {} sinks...",
				srcs.size(), sinks.size());
		int nSrc = srcs.size();
		if (!srcs.isEmpty() && !sinks.isEmpty()) {
			Set<Map.Entry<Statement, InterestingTaintNode>> s = srcs.entrySet();
			int idx = 1;
			for (Map.Entry<Statement, InterestingTaintNode> entry : s) {
				Statement stmt = entry.getKey();
				// if (!stmt.getNode()
				// .getMethod()
				// .getSignature()
				// .contains("RegistrationActivity.verifyAndSaveUserData(")) {
				// continue;
				// }
				InterestingTaintNode tn = entry.getValue();
				Graph<Statement> sdg = tn.enclosingTaintMap().sdg;
				logger.info("  * Check Source-{}: {}", idx++, stmt);
				TaintPath path = trackFromSource(tn.enclosingTaintSubMap(),
						sdg, stmt);

				logger.info("    - Get Result: {}", path == null ? "No" : "Yes");

				/*
				 * if (path != null) { if (path instanceof SimplifiedTaintPath)
				 * { // List<SimplifiedPath> l = ((SimplifiedTaintPath) //
				 * path).simplify(); ((SimplifiedTaintPath) path).simplify();
				 * try { String dotFile = String.format("%d.dot", idx);
				 * DotUtil.dotify(path, TaintPath.makeNodeDecorator(), dotFile,
				 * null, null); } catch (WalaException e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); } } }
				 */
				// for (SimplifiedPath p : l) {
				// BufferedWriter writer = null;
				// try {
				// writer = new BufferedWriter(new FileWriter(idx
				// + ".UI.txt"));
				// writer.write(p.toString());
				// writer.flush();
				// } catch (Exception ex) {
				//
				// } finally {
				// if (writer != null) {
				// try {
				// writer.close();
				// } catch (Exception ex) {
				//
				// }
				// }
				// }
				// idx++;
				// }
				// }
				// }
			}
		}
		logger.info("Finish Taint Track for {} Sources and "
				+ "Find Taint Paths for {} Sources.", nSrc, allPaths.size());
	}

	private TaintPath trackFromSource(TaintSubMap sm, Graph<Statement> sdg,
			Statement source) {
		SimplifiedTaintPath path = new SimplifiedTaintPath(source);
		trackFromStmt(sm, sdg, source, path, 0);
		trackCrossEntrypoint(0, path, new Stack<Entrypoint>());
		// System.err.println("****** " + path.getNumberOfNodes());
		if (path.hasSink()) {
			path.removeUnneccessaryNodes();
			// System.err.println("======= " + path.getNumberOfNodes());
			allPaths.add(path);
			return path;
		}
		return null;
	}

	private void trackFromStmt(TaintSubMap sm, Graph<Statement> sdg,
			Statement statement, TaintPath path, int permLevel) {
		Set<Statement> outgoingStmts = new HashSet<Statement>();

		trackFromStmt(sm, sdg, statement, path, outgoingStmts);

		Map<String, Set<Statement>> clusteredFieldPut = new HashMap<String, Set<Statement>>();
		for (Statement stmt : outgoingStmts) {
			Kind k = stmt.getKind();
			if (k == Kind.PARAM_CALLER) {
				crossEntrypointRecord.add(stmt);
				crossEntrypointRecord.add(sm);
				crossEntrypointRecord.add(Integer.valueOf(permLevel));
			} else if (k == Kind.NORMAL) {
				SSAPutInstruction pinst = (SSAPutInstruction) ((NormalStatement) stmt).getInstruction();
				String sig = pinst.getDeclaredField().getSignature();
				Set<Statement> s = clusteredFieldPut.get(sig);
				if (s == null) {
					s = new HashSet<Statement>();
					clusteredFieldPut.put(sig, s);
				}
				s.add(stmt);
			}
		}
		Collection<Set<Statement>> puts = clusteredFieldPut.values();
		for (Set<Statement> s : puts) {
			crossEntrypointRecord.add(s);
			crossEntrypointRecord.add(sm);
			crossEntrypointRecord.add(Integer.valueOf(permLevel));
		}
	}

	private void trackFromStmt(TaintSubMap sm, Graph<Statement> sdg,
			Statement stmt, TaintPath path, Set<Statement> outgoingStmts) {
		List<Statement> worklist = new LinkedList<Statement>();
		worklist.add(stmt);

		int count = 0;
		Set<TaintPathNode> allNewNodes = new HashSet<TaintPathNode>();
		while (!worklist.isEmpty()) {
			Statement next = worklist.remove(0);
			if (path.getPathNode(next) != null) {
				continue;
			}
			try {
				Iterator<Statement> iter = sdg.getSuccNodes(next);
				while (iter.hasNext()) {
					Statement succ = iter.next();
					worklist.add(succ);
				}
			} catch (Throwable e) {
				continue;
			}
			count++;
			if (count % 100 == 0) {
				// logger.debug("    - Processed {} Statements.", count);
			}
			// sink check must be before addPathNode in order to mark it as sink
			InterestingTaintNode itn = sinks.get(next);
			if (itn != null) {
				path.addSink(next, itn.tag);
			}
			TaintPathNode pn = path.addPathNode(next);
			allNewNodes.add(pn);
			Kind k = next.getKind();
			if (k == Kind.NORMAL) {
				SSAInstruction inst = ((NormalStatement) next).getInstruction();
				if (inst instanceof SSAPutInstruction) {
					outgoingStmts.add(next);
				}
			} else if (k == Kind.PARAM_CALLER) {
				ParamCaller pcstmt = (ParamCaller) next;
				if (interestingTransmissions.containsKey(pcstmt.getInstruction())) {
					outgoingStmts.add(next);
				}
			}
		}
		for (TaintPathNode pn : allNewNodes) {
			Statement st = pn.getStmt();
			Iterator<Statement> iter = sdg.getSuccNodes(st);
			while (iter.hasNext()) {
				Statement succ = iter.next();
				TaintPathNode p = path.getPathNode(succ);
				if (p != null)
					path.addEdge(pn, p);
			}
		}
	}

	private void trackCrossEntrypoint(int initialSize, TaintPath path,
			Stack<Entrypoint> visitedEntry) {
		while (crossEntrypointRecord.size() > initialSize + 2) {
			Object nstmt = crossEntrypointRecord.remove(initialSize);
			TaintSubMap sm = (TaintSubMap) crossEntrypointRecord.remove(initialSize);
			int permLevel = ((Integer) crossEntrypointRecord.remove(initialSize)).intValue();
			Set<Statement> stmtSet;
			if (nstmt instanceof Statement) {
				stmtSet = new HashSet<Statement>();
				stmtSet.add((Statement) nstmt);
			} else {
				stmtSet = (Set<Statement>) nstmt;
			}
			trackCrossEntrypointForOne(stmtSet, sm, permLevel, path,
					visitedEntry, crossEntrypointRecord.size());
		}
	}

	private Map<Pair<String, Entrypoint>, Integer> visitedEntrypointCrossing = new HashMap<Pair<String, Entrypoint>, Integer>();

	private void trackCrossEntrypointForOne(Set<Statement> stmtSet,
			TaintSubMap sm, int permLevel, TaintPath path,
			Stack<Entrypoint> visitedEntry, int sizeRecord) {
		Entrypoint ep = sm.taintMap.entry;
		visitedEntry.push(ep);
		if (permLevel < 3) {
			int nn = 0;
			Statement statement = stmtSet.iterator().next();
			if (statement.getKind() == Kind.NORMAL) {
				NormalStatement nstmt = (NormalStatement) statement;
				String sig = ((SSAPutInstruction) nstmt.getInstruction()).getDeclaredField()
						.getSignature();

				Set<Entry<Entrypoint, TaintMap>> set = entry2TaintMap.entrySet();
				int c = 0;
				for (Map.Entry<Entrypoint, TaintMap> entry : set) {
					TaintMap tm = entry.getValue();
					Entrypoint e = entry.getKey();
					c++;
					if (visitedEntry.contains(e)) {
						continue;
					}
					Pair<String, Entrypoint> pair = Pair.make(sig, e);
					Integer levelCounter = visitedEntrypointCrossing.get(pair);
					if (levelCounter != null
							&& permLevel >= levelCounter.intValue()) {
						continue;
					}
					path.setChanged(false);
					Iterator<Statement> iter = tm.iterateAllIncomingFields(sig);
					// if (iter.hasNext()) {
					// logger.debug(
					// "    + Go into [{}] {}.{}() for field propagation.",
					// permLevel, e.getMethod()
					// .getDeclaringClass()
					// .getName()
					// .toString(), e.getMethod()
					// .getName()
					// .toString(), permLevel);
					// logger.debug("     * {}", sig);
					// for (Entrypoint ept : visitedEntry) {
					// logger.debug("       # {}", ept.getMethod()
					// .getSignature());
					// }
					// }
					boolean hasCorrespondingFieldGet = iter.hasNext();
					if (hasCorrespondingFieldGet) {
						TaintPath newPath = new TaintPath();
						while (iter.hasNext()) {
							nn++;
							Statement stmt = iter.next();
							newPath.addFakeSource(stmt);
							trackFromStmt(
									tm.findOrCreateSubMap(stmt.getNode()),
									tm.sdg, stmt, newPath, permLevel + 1);
						}
						trackCrossEntrypoint(sizeRecord, newPath, visitedEntry);
						mergeTaintPath(path, stmtSet, newPath);
						newPath = null;
						if (/* hasCorrespondingFieldGet && */!path.isChanged()) {
							levelCounter = Integer.valueOf(permLevel);
							visitedEntrypointCrossing.put(pair, levelCounter);
						}
					}
					// while (iter.hasNext()) {
					// nn++;
					// Statement stmt = iter.next();
					// // if we continue use "path" instead of newPath, some
					// // sinks cannot be found for certain orders of
					// // entrypoint visit. e.g. src->1->2->3 discovers sink in
					// // (3) but src->1->2->4 does not discover any sink and
					// // when visiting as order src->1->2[->3] after
					// // src->1->2->4, we simply connect existing node in
					// // "path" and ignore following traversal even if we can
					// // find new node later.
					// TaintPath newPath = new TaintPath(stmt);
					// trackFromStmt(tm.findOrCreateSubMap(stmt.getNode()),
					// tm.sdg, stmt, newPath, permLevel + 1);
					// if (crossEntrypointRecord.size() > sizeRecord) {
					// logger.debug(" {} - {} : {}",
					// crossEntrypointRecord.size(), sizeRecord, permLevel);
					// }
					// trackCrossEntrypoint(sizeRecord, newPath, visitedEntry);
					// mergeTaintPath(path, stmtSet, newPath);
					// newPath = null;
					// }
					// if (hasCorrespondingFieldGet && !path.isChanged()) {
					// levelCounter = Integer.valueOf(permLevel);
					// visitedEntrypointCrossing.put(pair, levelCounter);
					// }
				}

			} else if (statement.getKind() == Kind.PARAM_CALLER) {
				String target = interestingTransmissions.get(((ParamCaller) statement).getInstruction());
				if (target != null) {
					Set<Entry<Entrypoint, TaintMap>> set = entry2TaintMap.entrySet();
					for (Map.Entry<Entrypoint, TaintMap> entry : set) {
						TaintMap tm = entry.getValue();
						Entrypoint e = entry.getKey();
						String sig = e.getMethod().getSignature();
						if (sig.equals(target)) {
							Pair<String, Entrypoint> pair = Pair.make(sig, ep);
							Integer levelCounter = visitedEntrypointCrossing.get(pair);
							if (levelCounter != null
									&& permLevel >= levelCounter.intValue()) {
								continue;
							}
							path.setChanged(false);
							Iterator<Statement> epEntry = tm.iterateEntries();
							// if (epEntry.hasNext()) {
							// logger.info(
							// "    + Go into {}.{}() for argument transmission ({}).",
							// e.getMethod()
							// .getDeclaringClass()
							// .getName()
							// .toString(), e.getMethod()
							// .getName()
							// .toString(), permLevel);
							// }
							boolean hasCorrespondingFieldGet = epEntry.hasNext();
							if (hasCorrespondingFieldGet) {
								TaintPath newPath = new TaintPath();
								while (epEntry.hasNext()) {
									Statement entryStmt = epEntry.next();
									newPath.addFakeSource(entryStmt);
									trackFromStmt(
											tm.findOrCreateSubMap(entryStmt.getNode()),
											tm.sdg, entryStmt, newPath,
											permLevel + 1);
								}
								trackCrossEntrypoint(sizeRecord, newPath,
										visitedEntry);
								mergeTaintPath(path, stmtSet, newPath);
								newPath = null;
								if (/* hasCorrespondingFieldGet && */!path.isChanged()) {
									levelCounter = Integer.valueOf(permLevel);
									visitedEntrypointCrossing.put(pair,
											levelCounter);
								}
							}
							// while (epEntry.hasNext()) {
							// nn++;
							// Statement entryStmt = epEntry.next();
							// TaintPath newPath = new TaintPath(entryStmt);
							// trackFromStmt(
							// tm.findOrCreateSubMap(entryStmt.getNode()),
							// tm.sdg, entryStmt, newPath,
							// permLevel + 1);
							// trackCrossEntrypoint(sizeRecord, newPath,
							// visitedEntry);
							// mergeTaintPath(path, stmtSet, newPath);
							// newPath = null;
							// }
							// if (hasCorrespondingFieldGet &&
							// !path.isChanged()) {
							// levelCounter = Integer.valueOf(permLevel);
							// visitedEntrypointCrossing.put(pair,
							// levelCounter);
							// }
						}
					}

				}
			}
			// logger.debug("[{}] : {}", sizeRecord,
			// ep.getMethod().getSignature());
			// logger.debug("[nn={}], Perm={}, {}", nn, permLevel,
			// statement.toString());
		}
		// while (visitedEntry.size() > permLevel)
		visitedEntry.pop();
	}

	private void mergeTaintPath(TaintPath original, Set<Statement> outs,
			TaintPath newer) {
		if (!newer.hasSink()) {
			return;
		}
		newer.removeUnneccessaryNodes();
		List<Statement> srcs = new LinkedList<Statement>();
		Statement stmt = newer.source;
		if (stmt != null) {
			srcs.add(stmt);
		}
		Iterator<Statement> iterFSrc = newer.iterateFakeSource();
		while (iterFSrc.hasNext()) {
			stmt = iterFSrc.next();
			srcs.add(stmt);
		}
		if (srcs.isEmpty()) {
			return;
		}
		boolean fakeSrcExists = false;
		List<TaintPathNode> worklist = new LinkedList<TaintPathNode>();
		while (!srcs.isEmpty()) {
			stmt = srcs.remove(0);
			TaintPathNode srcNode = newer.getPathNode(stmt);
			if (srcNode == null) {
				continue;
			}
			TaintPathNode oSrc = original.getPathNode(stmt);
			if (oSrc == null) {
				oSrc = original.addPathNode(stmt);
				worklist.add(oSrc);
			}
			for (Statement out : outs) {
				original.addEdge(original.getPathNode(out), oSrc);
			}
			original.setChanged(true);
			fakeSrcExists = true;
		}
		if (fakeSrcExists) {
			// worklist.clear();
			Iterator<TaintPathNode> iter = newer.iterator();
			while (iter.hasNext()) {
				TaintPathNode newPn = iter.next();
				Statement newStmt = newPn.getStmt();
				if (original.getPathNode(newStmt) == null) {
					TaintPathNode pn = original.addPathNode(newStmt);
					if (newPn.isSink()) {
						pn.setAsSink();
						original.addSink(newStmt, newer.getSinkTag(newStmt));
					}
					worklist.add(pn);
				}
			}
			while (!worklist.isEmpty()) {
				TaintPathNode pn = worklist.remove(0);
				Statement stmtPn = pn.getStmt();
				TaintPathNode newPn = newer.getPathNode(stmtPn);
				iter = newer.getPredNodes(newPn);
				while (iter.hasNext()) {
					TaintPathNode xyz = iter.next();
					Statement predStmt = xyz.getStmt();
					original.addEdge(original.getPathNode(predStmt), pn);
				}
				iter = newer.getSuccNodes(newPn);
				while (iter.hasNext()) {
					Statement succStmt = iter.next().getStmt();
					original.addEdge(pn, original.getPathNode(succStmt));
				}
			}
		}
		// while (!worklist.isEmpty()) {
		// TaintPathNode tpn = worklist.remove(0);
		// Iterator<TaintPathNode> iter = newer.getSuccNodes(tpn);
		// while (iter.hasNext()) {
		// TaintPathNode next = iter.next();
		// Statement nextStmt = next.getStmt();
		// TaintPathNode pn = original.getPathNode(nextStmt);
		// if (pn == null) {
		// pn = original.addPathNode(nextStmt);
		// if (next.isSink()) {
		// pn.setAsSink();
		// original.addSink(nextStmt, newer.getSinkTag(nextStmt));
		// }
		// worklist.add(next);
		// }
		// original.addEdge(original.getPathNode(tpn.getStmt()), pn);
		// }
		// }

	}

	public void visitSDG(Entrypoint ep, CallGraph cg, Graph<Statement> sdg) {
		currentTaintMap = new TaintMap(ep);
		entry2TaintMap.put(ep, currentTaintMap);
		logger.info("  - Visiting SDG");
		for (Statement stmt : sdg) {
			if (taskTimeout) {
				break;
			}
			visitStmt(cg, sdg, stmt);
		}
	}

	private void visitStmt(CallGraph cg, Graph<Statement> sdg, Statement stmt) {
		if (stmt.getNode().equals(cg.getFakeRootNode())) {
			return;
		}
		switch (stmt.getKind()) {
			case NORMAL:
				visitNormal(sdg, stmt);
				break;
			case PARAM_CALLEE:
				visitParamCallee(cg, sdg, stmt);
				break;
			case PARAM_CALLER:
				visitParamCaller(sdg, stmt);
				break;
			case NORMAL_RET_CALLER:
				visitNormalRetCaller(sdg, stmt);
				break;
			case PHI:
				visitPhi(sdg, stmt);
				break;
			default:
				break;
		}
	}

	private void visitNormal(Graph<Statement> sdg, Statement stmt) {
		NormalStatement nstmt = (NormalStatement) stmt;
		SSAInstruction inst = nstmt.getInstruction();

		if (inst instanceof SSAPutInstruction) {
			SSAPutInstruction pinst = (SSAPutInstruction) inst;
			currentTaintMap.collectOutgoingField(pinst.getDeclaredField()
					.getSignature(), nstmt);

		} else if (inst instanceof SSAGetInstruction) {
			SSAGetInstruction ginst = (SSAGetInstruction) inst;
			currentTaintMap.collectIncomingField(ginst.getDeclaredField()
					.getSignature(), nstmt);

			CGNode cn = stmt.getNode();
			TaintSubMap sm = currentTaintMap.findOrCreateSubMap(cn);
			sm.recordDislinkedPropagation(ginst.getRef(), stmt);
			if (false && ginst.getDeclaredFieldType().isArrayType()) {
				boolean onlyRecord = false;
				if (sdg.getSuccNodeCount(stmt) == 0) {
					// perhaps an arraystore is not connected as succ
					onlyRecord = true;
				}
				Iterator<Statement> preds = sdg.getPredNodes(stmt);
				while (preds.hasNext()) {
					Statement pred = preds.next();
					if (pred.getKind() == Kind.HEAP_PARAM_CALLEE) {
						HeapParamCallee hpc = (HeapParamCallee) pred;
						PointerKey pkey = hpc.getLocation();
						if (pkey instanceof InstanceFieldKey) {
							InstanceFieldKey ifKey = (InstanceFieldKey) pkey;
							if (!ifKey.getField()
									.getFieldTypeReference()
									.isArrayType()) {
								continue;
							}
						} else if (pkey instanceof StaticFieldKey) {
							StaticFieldKey sfKey = (StaticFieldKey) pkey;
							if (!sfKey.getField()
									.getFieldTypeReference()
									.isArrayType()) {
								continue;
							}
						} else {
							continue;
						}
						if (onlyRecord) {
							// possible write to the field via kind of
							// arraystore
							if (ginst.getDeclaredField()
									.getDeclaringClass()
									.getName()
									.toString()
									.startsWith("Ljava/lang/")) {
								System.err.format("%x <> %x : %s\n",
										pkey.hashCode(), stmt.getNode()
												.hashCode(), stmt.toString());
							}
							currentTaintMap.addArrayField(pkey, stmt, true);
							Iterator<Statement> correspondingFieldRef = currentTaintMap.iteratorArrayFields(
									pkey, false);
							while (correspondingFieldRef.hasNext()) {
								Statement st = correspondingFieldRef.next();
								sm.recordDislinkedPropagation(ginst.getDef(),
										st);
								System.err.println(stmt + "\n >> " + st);
							}
						} else {
							if (ginst.getDeclaredField()
									.getDeclaringClass()
									.getName()
									.toString()
									.startsWith("Ljava/lang/")) {
								System.err.format("%x ++ %x : %s\n",
										pkey.hashCode(), stmt.getNode()
												.hashCode(), stmt.toString());
							}
							// here we have a sad story
							currentTaintMap.addArrayField(pkey, stmt, false);
							Iterator<Statement> correspondingFieldRef = currentTaintMap.iteratorArrayFields(
									pkey, true);
							while (correspondingFieldRef.hasNext()) {
								NormalStatement st = (NormalStatement) correspondingFieldRef.next();
								SSAInstruction oldGInst = st.getInstruction();
								TaintSubMap tsm = currentTaintMap.findOrCreateSubMap(st.getNode());
								tsm.recordDislinkedPropagation(
										oldGInst.getDef(), stmt);
							}
						}
					}
				}
			}
		} else if (inst instanceof SSAArrayLoadInstruction) {
			SSAArrayLoadInstruction ainst = (SSAArrayLoadInstruction) inst;
			CGNode cn = stmt.getNode();
			TaintSubMap sm = currentTaintMap.findOrCreateSubMap(cn);
			sm.recordDislinkedPropagation(ainst.getArrayRef(), nstmt);
		} else if (inst instanceof SSAReturnInstruction) {
			SSAReturnInstruction rinst = (SSAReturnInstruction) inst;
			if (!rinst.returnsVoid() && !rinst.returnsPrimitiveType()) {
				CGNode cn = stmt.getNode();
				TaintSubMap sm = currentTaintMap.findOrCreateSubMap(cn);
				sm.recordDislinkedPropagation(rinst.getResult(), stmt);
			}
		}
	}

	private void visitParamCallee(CallGraph cg, Graph<Statement> sdg,
			Statement stmt) {
		ParamCallee pc = (ParamCallee) stmt;
		if (pc.getValueNumber() != 1) {
			Iterator<CGNode> iter = cg.getEntrypointNodes().iterator();
			if (iter.hasNext() && pc.getNode().equals(iter.next())) {
				if (pc.getNode()
						.getMethod()
						.getName()
						.toString()
						.equals("doInBackground")) {
					currentTaintMap.addEntry(stmt);
				}
			}
		}
	}

	private boolean specialType(SSAAbstractInvokeInstruction inst, int val) {
		MethodReference mRef = inst.getDeclaredTarget();
		int nParam = inst.getNumberOfUses();
		for (int i = 0; i < nParam; i++) {
			if (val == inst.getUse(i)) {
				TypeReference tRef;
				if (inst.isStatic()) {
					tRef = mRef.getParameterType(i);
				} else {
					if (i == 0) {
						tRef = mRef.getDeclaringClass();
					} else {
						tRef = mRef.getParameterType(i - 1);
					}
				}
				if (tRef.isArrayType() || tRef.isReferenceType())
					// || (tRef.isReferenceType() && !tRef.getName()
					// .toString()
					// .startsWith("Ljava/lang/")))
					return true;
				else if (tRef.getClassLoader().equals(
						ClassLoaderReference.Primordial))
					return true;
				break;
			}
		}
		return false;
	}

	private void visitParamCaller(Graph<Statement> sdg, Statement stmt) {
		CGNode cn = stmt.getNode();
		TaintSubMap sm = currentTaintMap.findOrCreateSubMap(cn);
		ParamCaller pcstmt = (ParamCaller) stmt;
		SSAAbstractInvokeInstruction inst = pcstmt.getInstruction();
		int nsucc = sdg.getSuccNodeCount(pcstmt);
		int pval = pcstmt.getValueNumber();
		if (nsucc == 0 && !(tryRecordTaintSink(inst, sm, cha, pcstmt))) {
			TaintModel.checkModel(sdg, pcstmt, interestingTransmissions);
		}
		// why use specialInterests() judgement here? it is used when building
		// summary. seems useless here.
		if (specialType(inst, pval)) {
			if (!inst.isStatic()) {
				int thisVal = inst.getUse(0);
				if (pval != thisVal) {
					sm.recordDislinkedPropagation(pval, pcstmt);
				}
			} else {
				sm.recordDislinkedPropagation(pval, pcstmt);
			}
		}
	}

	private void visitNormalRetCaller(Graph<Statement> sdg, Statement stmt) {
		CGNode cn = stmt.getNode();
		TaintSubMap sm = currentTaintMap.findOrCreateSubMap(cn);
		NormalReturnCaller nstmt = (NormalReturnCaller) stmt;
		SSAAbstractInvokeInstruction inst = nstmt.getInstruction();
		int npred = sdg.getPredNodeCount(nstmt);
		if (npred == 0) {
			if (tryRecordTaintSink(inst, sm, cha, nstmt)) {
				return;
			} else if (!tryRecordTaintSrc(inst, sm, cha, nstmt)) {
				int nparam = inst.getNumberOfParameters();
				for (int i = 0; i < nparam; i++) {
					sm.recordDislinkedPropagation(inst.getUse(i), nstmt);
				}
			}
		}
	}

	private void visitPhi(Graph<Statement> sdg, Statement stmt) {
		PhiStatement phi = (PhiStatement) stmt;
		SSAPhiInstruction phiInst = phi.getPhi();
		TaintSubMap sm = currentTaintMap.findOrCreateSubMap(stmt.getNode());
		int nUse = phiInst.getNumberOfUses();
		for (int i = 0; i < nUse; i++) {
			sm.recordDislinkedPropagation(phiInst.getUse(i), stmt);
		}
	}

	/**
	 * 
	 * @param instr
	 * @param sm
	 * @param cha
	 * @param nstmt
	 * @return true - src; false - non src.
	 */
	private boolean tryRecordTaintSrc(SSAAbstractInvokeInstruction instr,
			TaintSubMap sm, ClassHierarchy cha, NormalReturnCaller nstmt) {
		String sig = WalaUtil.getSignature(instr);
		String intestringIndices = AnalysisConfig.getPotentialSrc(sig);
		if (intestringIndices != null) {
			InterestingTaintNode node = InterestingTaintNode.getInstance(instr,
					sm, "UI,-1");
			srcsForCurrentEntry.put(nstmt, node);
			logger.info("SOURCE: {}->{}() in [{}.{}()]",
					instr.getDeclaredTarget()
							.getDeclaringClass()
							.getName()
							.toString(), instr.getDeclaredTarget()
							.getName()
							.toString(), sm.cgNode.getMethod()
							.getDeclaringClass()
							.getName()
							.toString(), sm.cgNode.getMethod()
							.getName()
							.toString());
			return true;
		}
		return false;
	}

	private boolean tryRecordTaintSink(SSAAbstractInvokeInstruction instr,
			TaintSubMap sm, ClassHierarchy cha, NormalReturnCaller nstmt) {
		String sig = WalaUtil.getSignature(instr);
		String intestringIndices = AnalysisConfig.getPotentialSink(sig);
		if (intestringIndices != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param instr
	 * @param sm
	 * @param cha
	 * @param pstmt
	 * @return true - sink; false - non sink
	 */
	private boolean tryRecordTaintSink(SSAAbstractInvokeInstruction instr,
			TaintSubMap sm, ClassHierarchy cha, ParamCaller pstmt) {
		String sig = WalaUtil.getSignature(instr);
		int pval = pstmt.getValueNumber();
		String intestringIndices = AnalysisConfig.getPotentialSink(sig);
		if (intestringIndices != null) {
			boolean pvalMatchSinkArg = false;
			List<String> l = Arrays.asList(intestringIndices.split(AnalysisConfig.SEPERATOR));
			for (int i = 1; i < l.size(); i++) {
				int useAtI = Integer.parseInt(l.get(i));
				if (pval == instr.getUse(useAtI)) {
					pvalMatchSinkArg = true;
					break;
				}
			}
			if (pvalMatchSinkArg) {
				InterestingTaintNode node = InterestingTaintNode.getInstance(
						instr, sm, intestringIndices);
				sinksForCurrentEntry.put(pstmt, node);
				logger.info("SINK: {}->{}() in [{}.{}()]",
						instr.getDeclaredTarget()
								.getDeclaringClass()
								.getName()
								.toString(), instr.getDeclaredTarget()
								.getName()
								.toString(), sm.cgNode.getMethod()
								.getDeclaringClass()
								.getName()
								.toString(), sm.cgNode.getMethod()
								.getName()
								.toString());
				return true;
			}
		}
		return false;
	}

	private Graph<Statement> pruneSDG(final SDG sdg) {
		return GraphSlicer.prune(sdg, new Predicate<Statement>() {

			@Override
			public boolean test(Statement t) {
				Statement.Kind k = t.getKind();
				/*
				 * if (t.getNode().equals(sdg.getCallGraph().getFakeRootNode()))
				 * { logger.debug("FakeRootNode: {}", k ); return false; } else
				 */if (k == Statement.Kind.METHOD_ENTRY
						|| k == Statement.Kind.METHOD_EXIT) {
					return false;
				} else if (t.getNode()
						.getMethod()
						.getDeclaringClass()
						.getClassLoader()
						.getReference()
						.equals(ClassLoaderReference.Primordial)
						&& (!t.getNode().getMethod().isSynthetic() || t.getNode()
								.getMethod()
								.getDeclaringClass()
								.getReference()
								.getName()
								.toString()
								.startsWith("Ljava/lang/"))) {
					return false;

				} else if (t.getNode()
						.getMethod()
						.getDeclaringClass()
						.getName()
						.toString()
						.startsWith("Landroid/support/v")) {
					return false;
				} else if (k == Statement.Kind.NORMAL) {
					NormalStatement ns = (NormalStatement) t;
					SSAInstruction inst = ns.getInstruction();
					if (inst instanceof SSAAbstractInvokeInstruction) {
						return false;
					} else if (inst instanceof SSAGetInstruction) {
						SSAGetInstruction getInst = (SSAGetInstruction) inst;
						if (getInst.isStatic()
								&& getInst.getDeclaredField()
										.getDeclaringClass()
										.getName()
										.toString()
										.equals("Ljava/lang/System")) {
							return false;
						}
					}
				} else if (k == Statement.Kind.PARAM_CALLER) {
					if (sdg.getPredNodeCount(t) == 0) {
						return false;
					}
				} else if (t instanceof HeapStatement) {
					HeapStatement hs = (HeapStatement) t;
					PointerKey pk = hs.getLocation();
					if (pk instanceof StaticFieldKey) {
						StaticFieldKey sfk = (StaticFieldKey) pk;
						if (sfk.getField()
								.getDeclaringClass()
								.getClassLoader()
								.getReference()
								.equals(ClassLoaderReference.Primordial)
								&& sfk.getField()
										.getDeclaringClass()
										.getName()
										.toString()
										.equals("Ljava/lang/System")) {
							return false;
						}
					}
				}

				return true;
			}
		});
	}
}
