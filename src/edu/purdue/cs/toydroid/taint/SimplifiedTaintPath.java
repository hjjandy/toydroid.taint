package edu.purdue.cs.toydroid.taint;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.wala.ipa.slicer.NormalReturnCaller;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Statement.Kind;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.graph.impl.SparseNumberedGraph;
import com.ibm.wala.util.graph.traverse.SlowDFSDiscoverTimeIterator;
import com.ibm.wala.viz.DotUtil;

public class SimplifiedTaintPath extends TaintPath {
	private static Logger logger = LogManager.getLogger(SimplifiedTaintPath.class);

	public class SimplifiedPath {
		public String strSource;
		public String strSink;
		public String sinkTag;
		public boolean hasToArray = false;
		private Pair<String, String> key;
		private Set<String> interestingNodes;

		SimplifiedPath(Stack<TaintPathNode> singlePath) {
			interestingNodes = new HashSet<String>();
			simplifyPath(singlePath);
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("**SOURCE**\n");
			builder.append(strSource);
			builder.append("\n\n##SINK##\n");
			builder.append(strSink);
			builder.append("\n");
			if (!interestingNodes.isEmpty()) {
				builder.append("\n$$Interesting Path Nodes$$\n");
				for (String str : interestingNodes) {
					builder.append(str);
					builder.append("\n\n");
				}
			}
			return builder.toString();
		}

		private void simplifyPath(Stack<TaintPathNode> singlePath) {
			// try {
			// BufferedWriter writer = new BufferedWriter(new FileWriter(
			// TaintModel.pathIdx + ".path"));
			// TaintModel.pathIdx++;

			// SparseNumberedGraph<TaintPathNode> PATH = new
			// SparseNumberedGraph<TaintPathNode>();
			// TaintPathNode lastTPN = null;
			while (!singlePath.isEmpty()) {
				TaintPathNode tpn = singlePath.pop();
				// PATH.addNode(tpn);
				// if (lastTPN != null) {
				// PATH.addEdge(tpn, lastTPN);
				// }
				// lastTPN = tpn;
				String x;
				if (tpn.isSource()) {
					strSource = convertStmtToString(tpn.getStmt(), true);
					x = strSource;
				} else if (tpn.isSink()) {
					strSink = convertStmtToString(tpn.getStmt(), true);
					x = strSink;
					sinkTag = sink.get(tpn.getStmt());
				} else {
					String str = checkInterestingPathNode(tpn.getStmt());
					x = str;
					if (str != null) {
						interestingNodes.add(str);
					}
				}

				// if (x == null) x =tpn.getStmt().toString();
				// writer.write(x);
				// writer.newLine();
			}//

			// try {
			// DotUtil.dotify(PATH, TaintPath.makeNodeDecorator(), "p"
			// + TaintModel.pathIdx + ".dot", null, null);
			// } catch (WalaException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// TaintModel.pathIdx ++;
			//
			// writer.flush();
			// writer.close();
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
		}

		public Set<String> getInterestingNodes() {
			return interestingNodes;
		}

		public Pair<String, String> keyAsSourceAndSink() {
			if (key == null) {
				key = Pair.make(strSource, strSink);
			}
			return key;
		}

		public boolean merge(SimplifiedPath anotherPath) {
			if (strSource != null && strSource.equals(anotherPath.strSource)
					&& strSink != null && strSink.equals(anotherPath.strSink)) {
				interestingNodes.addAll(anotherPath.interestingNodes);
				hasToArray = (hasToArray | anotherPath.hasToArray);
				return true;
			}
			return false;
		}
	}

	private List<SimplifiedPath> paths;

	public SimplifiedTaintPath(Statement src) {
		super(src);
	}

	public List<SimplifiedPath> getPaths() {
		return paths;
	}

	private void dumpSinglePath(Stack<TaintPathNode> path) {

	}

	public List<SimplifiedPath> simplify() {
		logger.info("    - Simplify the taint path [{} sinks].", sink.size());
		paths = new LinkedList<SimplifiedPath>();
		SlowDFSDiscoverTimeIterator<TaintPathNode> dfsIter = new SlowDFSDiscoverTimeIterator<TaintPathNode>(
				this, getPathNode(source));
		Stack<TaintPathNode> singlePath = new Stack<TaintPathNode>();
		boolean hasToArray = false;
		while (dfsIter.hasNext()) {
			singlePath.clear();
			ListIterator<TaintPathNode> listIter = dfsIter.listIterator();
			while (listIter.hasNext()) {
				singlePath.push(listIter.next());
			}
			TaintPathNode tpn = dfsIter.next();
			// ///////
			Statement ss = tpn.getStmt();
			if (ss.getKind() == Kind.NORMAL_RET_CALLER) {
				NormalReturnCaller nrc = (NormalReturnCaller) ss;
				if (nrc.getInstruction()
						.getDeclaredTarget()
						.getDeclaringClass()
						.getName()
						.toString()
						.startsWith("Ljava/util/")
						&& nrc.getInstruction()
								.getDeclaredTarget()
								.getName()
								.toString()
								.equals("toArray")) {
					hasToArray = true;
				}
			} else if (ss.getKind() == Kind.PARAM_CALLER) {
				ParamCaller nrc = (ParamCaller) ss;
				if (nrc.getInstruction()
						.getDeclaredTarget()
						.getDeclaringClass()
						.getName()
						.toString()
						.startsWith("Ljava/util/")
						&& nrc.getInstruction()
								.getDeclaredTarget()
								.getName()
								.toString()
								.equals("toArray")) {
					hasToArray = true;
				}
			} else if (ss.getKind() == Kind.PARAM_CALLEE
					|| ss.getKind() == Kind.NORMAL_RET_CALLEE
					|| ss.getKind() == Kind.NORMAL) {
				if (ss.getNode()
						.getMethod()
						.getDeclaringClass()
						.getName()
						.toString()
						.startsWith("Ljava/util/")
						&& ss.getNode()
								.getMethod()
								.getName()
								.toString()
								.equals("toArray")) {
					hasToArray = true;
				}
			}
			// ///////
			if (tpn.isSink()) {
				SimplifiedPath p = new SimplifiedPath(singlePath);
				paths.add(p);
			}
		}
		if (hasToArray) {
			for (SimplifiedPath sp : paths) {
				sp.hasToArray = true;
			}
		}
		return paths;
	}

	public void dumpSpecificPath() {
		// for ()
	}

	protected String checkInterestingPathNode(Statement s) {
		Kind k = s.getKind();
		if (k == Kind.PARAM_CALLER) {
			SSAAbstractInvokeInstruction inst = ((ParamCaller) s).getInstruction();
			String klass = inst.getDeclaredTarget()
					.getDeclaringClass()
					.getName()
					.toString();
			if (TaintModel.isInterestingModel(klass)) {
				return convertInterestingPathNode(s);
			}
		} else if (k == Kind.PARAM_CALLEE || k == Kind.NORMAL_RET_CALLEE) {
			String klass = s.getNode()
					.getMethod()
					.getDeclaringClass()
					.getName()
					.toString();
			if (TaintModel.isInterestingModel(klass)) {
				return convertInterestingPathNode(s);
			}
		} else if (k == Kind.NORMAL_RET_CALLER) {
			SSAAbstractInvokeInstruction inst = ((NormalReturnCaller) s).getInstruction();
			String klass = inst.getDeclaredTarget()
					.getDeclaringClass()
					.getName()
					.toString();
			if (TaintModel.isInterestingModel(klass)) {
				return convertInterestingPathNode(s);
			}
		}
		return null;
	}

	private String convertStmtToString(Statement stmt, boolean isSrcOrSink) {
		StringBuilder builder = new StringBuilder();
		SSAAbstractInvokeInstruction inst = null;
		if (stmt.getKind() == Kind.NORMAL_RET_CALLER) {
			inst = ((NormalReturnCaller) stmt).getInstruction();
		} else if (stmt.getKind() == Kind.PARAM_CALLER) {
			inst = ((ParamCaller) stmt).getInstruction();
		}
		if (inst != null) {
			builder.append(stmt.getKind());
			builder.append(":");
			if (isSrcOrSink) {
				if (inst.hasDef()) {
					builder.append("[v");
					builder.append(inst.getDef());
					builder.append("]=");
				}
				int nParam = inst.getNumberOfUses();
				builder.append("{");
				for (int i = 0; i < nParam; i++) {
					builder.append("v");
					builder.append(inst.getUse(i));
					if (i != nParam - 1) {
						builder.append(",");
					}
				}
				builder.append("}");
				if (stmt.getKind() == Kind.PARAM_CALLER) {
					ParamCaller pc = (ParamCaller) stmt;
					builder.append("/v");
					builder.append(pc.getValueNumber());
				}
			}
			builder.append(": ");
			builder.append(inst.getDeclaredTarget().getSignature());
			builder.append("\n  in [");
			builder.append(stmt.getNode().getMethod().getSignature());
			builder.append("]");
		}
		return builder.toString();
	}

	public String convertInterestingPathNode(Statement stmt) {
		String strNode = null;
		if (stmt.getKind() == Kind.NORMAL_RET_CALLER
				|| stmt.getKind() == Kind.PARAM_CALLER) {
			strNode = convertStmtToString(stmt, false);
		} else if (stmt.getKind() == Kind.PARAM_CALLEE) {
			StringBuilder builder = new StringBuilder("Callee  as [");
			builder.append(stmt.getNode().getMethod().getSignature());
			builder.append("]");
			strNode = builder.toString();
		} else if (stmt.getKind() == Kind.NORMAL_RET_CALLEE) {
			StringBuilder builder = new StringBuilder("Return in Callee [");
			builder.append(stmt.getNode().getMethod().getSignature());
			builder.append("]");
			strNode = builder.toString();
		}
		return strNode;
	}

}
