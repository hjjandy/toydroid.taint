package edu.purdue.cs.toydroid.taint;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Statement.Kind;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.util.graph.Graph;

public class TaintMap {
	private static Logger logger = LogManager.getLogger(TaintMap.class);

	public Entrypoint entry;
	public Graph<Statement> sdg;
	private Map<String, Set<Statement>> fieldIncoming; // possible
	// incoming
	// fields
	private Map<String, Set<Statement>> fieldOutgoing; // possible
	// outgoing
	// fields
	private Set<Statement> entries;
	private Map<CGNode, TaintSubMap> subMaps;
	private Map<PointerKey, Set<Statement>> arrayFieldsWrite, arrayFieldsRead;

	public TaintMap(Entrypoint e) {
		entry = e;
		fieldIncoming = new HashMap<String, Set<Statement>>();
		fieldOutgoing = new HashMap<String, Set<Statement>>();
		entries = new HashSet<Statement>();
		subMaps = new HashMap<CGNode, TaintSubMap>();
		arrayFieldsWrite = new HashMap<PointerKey, Set<Statement>>();
		arrayFieldsRead = new HashMap<PointerKey, Set<Statement>>();
	}

	public TaintSubMap findOrCreateSubMap(CGNode n) {
		TaintSubMap sm = subMaps.get(n);
		if (sm == null) {
			sm = new TaintSubMap(n, this);
			subMaps.put(n, sm);
		}
		return sm;
	}

	public void removeIfExists(Statement stmt) {
		entries.remove(stmt);
		if (stmt.getKind() == Kind.NORMAL) {
			NormalStatement nstmt = (NormalStatement) stmt;
			SSAInstruction inst = nstmt.getInstruction();
			if (inst instanceof SSAGetInstruction) {
				String sig = ((SSAGetInstruction) inst).getDeclaredField()
						.getSignature();
				Set<Statement> set = fieldIncoming.get(sig);
				if (set != null) {
					set.remove(stmt);
					if (set.isEmpty()) {
						fieldIncoming.remove(sig);
					}
				}
			}
		}
	}

	public void addArrayField(PointerKey pkey, Statement stmt, boolean write) {
		Set<Statement> set;
		if (write)
			set = arrayFieldsWrite.get(pkey);
		else
			set = arrayFieldsRead.get(pkey);
		if (set == null) {
			set = new HashSet<Statement>();
			if (write)
				arrayFieldsWrite.put(pkey, set);
			else
				arrayFieldsRead.put(pkey, set);
		}
		set.add(stmt);
	}

	public Iterator<Statement> iteratorArrayFields(PointerKey pkey,
			boolean write) {
		Set<Statement> set;
		if (write)
			set = arrayFieldsWrite.get(pkey);
		else
			set = arrayFieldsRead.get(pkey);
		if (set == null) {
			set = Collections.emptySet();
		}
		return set.iterator();
	}

	public boolean hasEntries() {
		return !entries.isEmpty();
	}

	public boolean hasIncomingFields() {
		return !fieldIncoming.isEmpty();
	}

	public boolean hasOutgoingFields() {
		return !fieldOutgoing.isEmpty();
	}

	public void addEntry(Statement stmt) {
		entries.add(stmt);
	}

	public void collectOutgoingField(String sig, Statement stmt) {
		Set<Statement> s = fieldOutgoing.get(sig);
		if (s == null) {
			s = new HashSet<Statement>();
			fieldOutgoing.put(sig, s);
		}
		s.add(stmt);
	}

	public void collectIncomingField(String sig, Statement stmt) {
		Set<Statement> s = fieldIncoming.get(sig);
		if (s == null) {
			s = new HashSet<Statement>();
			fieldIncoming.put(sig, s);
		}
		s.add(stmt);
	}

	public Iterator<Statement> iterateEntries() {
		return entries.iterator();
	}

	public Iterator<Statement> iterateAllIncomingFields(String sig) {
		Set<Statement> s = fieldIncoming.get(sig);
		if (s == null) {
			s = Collections.emptySet();
		}
		return s.iterator();
	}

	public Iterator<Statement> iterateAllOutgoingFields(String sig) {
		Set<Statement> s = fieldOutgoing.get(sig);
		if (s == null) {
			s = Collections.emptySet();
		}
		return s.iterator();
	}

	public Iterator<Statement> iterateAllIncomingFields() {
		Set<Statement> ret = new HashSet<Statement>();
		Collection<Set<Statement>> s = fieldIncoming.values();
		for (Set<Statement> ss : s) {
			ret.addAll(ss);
		}
		return ret.iterator();
	}

	public Iterator<Statement> iterateAllOutgoingFields() {
		Set<Statement> ret = new HashSet<Statement>();
		Collection<Set<Statement>> s = fieldOutgoing.values();
		for (Set<Statement> ss : s) {
			ret.addAll(ss);
		}
		return ret.iterator();
	}
}