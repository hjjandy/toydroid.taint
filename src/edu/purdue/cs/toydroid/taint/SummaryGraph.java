package edu.purdue.cs.toydroid.taint;

import com.ibm.wala.util.graph.AbstractNumberedGraph;
import com.ibm.wala.util.graph.NumberedEdgeManager;
import com.ibm.wala.util.graph.NumberedNodeManager;
import com.ibm.wala.util.graph.impl.SlowNumberedNodeManager;
import com.ibm.wala.util.graph.impl.SparseNumberedEdgeManager;

public class SummaryGraph<T> extends AbstractNumberedGraph<T> {
	private NumberedNodeManager<T> nodeMgr = new SlowNumberedNodeManager<T>();
	private NumberedEdgeManager<T> edgeMgr = new SparseNumberedEdgeManager<T>(
			nodeMgr);

	@Override
	protected NumberedNodeManager<T> getNodeManager() {
		return nodeMgr;
	}

	@Override
	protected NumberedEdgeManager<T> getEdgeManager() {
		return edgeMgr;
	}


}
