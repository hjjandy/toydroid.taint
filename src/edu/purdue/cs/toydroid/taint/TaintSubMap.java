package edu.purdue.cs.toydroid.taint;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.Statement;

public class TaintSubMap {
	public CGNode cgNode;
	public TaintMap taintMap;
	private Map<Integer, Set<Statement>> propagations;

	public TaintSubMap(CGNode n, TaintMap tm) {
		cgNode = n;
		taintMap = tm;
		propagations = new HashMap<Integer, Set<Statement>>();
	}

	public void recordDislinkedPropagation(int val, Statement stmt) {
		Integer iObj = Integer.valueOf(val);
		Set<Statement> s = propagations.get(iObj);
		if (s == null) {
			s = new HashSet<Statement>();
			propagations.put(iObj, s);
		}
		s.add(stmt);
	}
	
	public Iterator<Statement> iterateDislinkedPropagation(int val) {
		Set<Statement> s = propagations.get(val);
		if (s == null) {
			s = Collections.emptySet();
		}
		return s.iterator();
	}
}
